package ba.bild.tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CoolClassTest {

	CoolClass cc;

	@Before
	public void setUp() throws Exception {
		cc = new CoolClass();
	}

	@Test
	public void testReturnZero() {
		// test data
		int expected = 0;

		// execute method
		int result = cc.returnZero();

		// assert
		assertEquals(expected, result);
	}

}
